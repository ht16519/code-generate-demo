package com.xh.generate.utils;

import java.io.File;

/**
 * @Name FileUtil
 * @Description 文件工具类
 * @Author wen
 * @Date 2020-03-29
 */
public class FileUtil {

    public static void createDirectory(String poPath) {
        File poDirectory = new File(poPath);
        if(!poDirectory.exists()){
            poDirectory.mkdirs();
        }
    }

}
