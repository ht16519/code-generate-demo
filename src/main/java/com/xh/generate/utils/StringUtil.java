package com.xh.generate.utils;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.*;

/**
 * @Name StringUtil
 * @Description
 * @Author wen
 * @Date 2020-03-29
 */
public class StringUtil {

    public static String packagePath2FilePath(String rootPath, String packagePath){
        return rootPath + packagePath.replace(".", "/") + "/";
    }

    public static String compensatePackage(String basePackage, String packagePath, String target){
        return isEmpty(packagePath) ? basePackage + target : packagePath;
    }

    public static boolean isEmpty(String target){
        return (target == null || target.trim().length() == 0);
    }

    public static boolean isNotEmpty(String target){
        return !isEmpty(target);
    }

    /**
     * 将目标字符串首字母变为大写
     * @param str 目标字符串
     * @return 首字母变为大写的字符串
     */
    public static String firstChar2UpperCase(String str) {
        //abcd-->Abcd
        //abcd-->ABCD-->Abcd
        return str.toUpperCase().substring(0, 1) + str.substring(1);
    }

    /**
     * @Name firstChar2LowerCase
     * @Description 将目标字符串首字母变为小写
     * @param str
     * @return java.lang.String
     */
    public static String firstChar2LowerCase(String str) {
        return str.toLowerCase().substring(0, 1) + str.substring(1);
    }

    /**
     * 将目标字符串首字符以及下划线转为驼峰
     *
     * @param str 目标字符串
     * @return 首字母变为大写的字符串
     */
    public static String firstAndToCamel(String str) {
        //abcd-->Abcd
        //abcd-->ABCD-->Abcd
        return toCamel(firstChar2UpperCase(str));
    }

    /**
     * 下划线转驼峰
     * @param str
     * @return
     */
    public static String toCamel(String str) {
        //利用正则删除下划线，把下划线后一位改成大写
        Pattern pattern = compile("_(\\w)");
        Matcher matcher = pattern.matcher(str);
        StringBuilder sb = new StringBuilder(str);
        if (matcher.find()) {
            sb = new StringBuilder();
            //将当前匹配子串替换为指定字符串，并且将替换后的子串以及其之前到上次匹配子串之后的字符串段添加到一个StringBuffer对象里。
            //正则之前的字符和被替换的字符
            matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
            //把之后的也添加到StringBuffer对象里
            matcher.appendTail(sb);
        } else {
            return sb.toString();
        }
        return toCamel(sb.toString());
    }

    /**
     * 驼峰转下划线
     * @param str
     * @return
     */
    public static String underline(String str) {
        Pattern pattern = compile("[A-Z]");
        Matcher matcher = pattern.matcher(str);
        StringBuilder sb = new StringBuilder(str);
        if (matcher.find()) {
            sb = new StringBuilder();
            //将当前匹配子串替换为指定字符串，并且将替换后的子串以及其之前到上次匹配子串之后的字符串段添加到一个StringBuffer对象里。
            //正则之前的字符和被替换的字符
            matcher.appendReplacement(sb, "_" + matcher.group(0).toLowerCase());
            //把之后的也添加到StringBuffer对象里
            matcher.appendTail(sb);
        } else {
            return sb.toString();
        }
        return underline(sb.toString());
    }
}
