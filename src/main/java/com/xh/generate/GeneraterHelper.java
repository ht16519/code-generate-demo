package com.xh.generate;

import com.xh.generate.core.CoreGenerater;
import com.xh.generate.core.PlugsGenerater;

/**
 * @Name GeneraterHelper
 * @Description 代码生成入口方法类
 * @Author wen
 * @Date 2020/3/31
 */
public class GeneraterHelper {


    public static void exec() {
        try {
            //生成核心代码
            CoreGenerater.generateCore();
            //生成项目config配置文件
            CoreGenerater.generateConfig();
            //生成ApiResult.java
            PlugsGenerater.generateApiResult();
            //生成commons代码
            PlugsGenerater.generateCommons();
            //生成 pom.xml 和 application.yml 文件
            PlugsGenerater.generatePomAndYml();
            System.out.println(" ----- BUILD SUCCESS!!! ----- ");
        } catch (Exception e) {
            System.out.println(" ----- BUILD FAILED!!! ----- ");
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        GeneraterHelper.exec();
    }

}
