package com.xh.generate.bean;


/**
 * 管理配置信息
 *
 * @a
 */
public class Configuration {
    /**
     * 驱动类
     */
    private String driver;
    /**
     * jdbc的url
     */
    private String url;
    /**
     * 数据库的用户名
     */
    private String username;
    /**
     * 数据库的密码
     */
    private String password;
    /**
     * 项目基础路径
     */
    private String basePackage;
    /**
     * 设置数据库表命名前缀
     */
    private String tableNamePrefix;
    /**
     * 项目classesPath
     */
    private String classesPath;
    /**
     * 项目生成rootPath
     */
    private String rootPath;
    /**
     * 代码生成磁盘路径（选填，默认本项目路径）
     */
    private String diskUrl;
    /**
     * 加载类型转换器类全路径
     */
    private String typeConvertorClassPath;
    /**
     * 需要生成的表（表名之间,分隔）
     */
    private String generateTables;
    /**
     * 是否生成commons公共代码（true | false 默认:true）
     */
    private String isGenerateCommonsCode;

    /**
     * 是否生成pom的dependencies依赖和yml文件（true | false 默认:true）
     * */
    private String isGeneratePomDependenciesAndYml;
    /**
     * 保存生成代码的所有类包路径
     */
    private PackagePath packagePath = new PackagePath();
    /**
     * 保存生成代码的所有文件路径
     */
    private CodeFilePath codeFilePath = new CodeFilePath();

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBasePackage() {
        return basePackage;
    }

    public void setBasePackage(String basePackage) {
        this.basePackage = basePackage;
    }

    public String getTableNamePrefix() {
        return tableNamePrefix;
    }

    public void setTableNamePrefix(String tableNamePrefix) {
        this.tableNamePrefix = tableNamePrefix;
    }

    public String getClassesPath() {
        return classesPath;
    }

    public void setClassesPath(String classesPath) {
        this.classesPath = classesPath;
    }

    public String getRootPath() {
        return rootPath;
    }

    public void setRootPath(String rootPath) {
        this.rootPath = rootPath;
    }

    public String getTypeConvertorClassPath() {
        return typeConvertorClassPath;
    }

    public void setTypeConvertorClassPath(String typeConvertorClassPath) {
        this.typeConvertorClassPath = typeConvertorClassPath;
    }

    public String getGenerateTables() {
        return generateTables;
    }

    public void setGenerateTables(String generateTables) {
        this.generateTables = generateTables;
    }

    public String getIsGenerateCommonsCode() {
        return isGenerateCommonsCode;
    }

    public void setIsGenerateCommonsCode(String isGenerateCommonsCode) {
        this.isGenerateCommonsCode = isGenerateCommonsCode;
    }

    public PackagePath getPackagePath() {
        return packagePath;
    }

    public void setPackagePath(PackagePath packagePath) {
        this.packagePath = packagePath;
    }

    public CodeFilePath getCodeFilePath() {
        return codeFilePath;
    }

    public void setCodeFilePath(CodeFilePath codeFilePath) {
        this.codeFilePath = codeFilePath;
    }

    public String getIsGeneratePomDependenciesAndYml() {
        return isGeneratePomDependenciesAndYml;
    }

    public void setIsGeneratePomDependenciesAndYml(String isGeneratePomDependenciesAndYml) {
        this.isGeneratePomDependenciesAndYml = isGeneratePomDependenciesAndYml;
    }


    public String getDiskUrl() {
        return diskUrl;
    }

    public void setDiskUrl(String diskUrl) {
        this.diskUrl = diskUrl;
    }
}


