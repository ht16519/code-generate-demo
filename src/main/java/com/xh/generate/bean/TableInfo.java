package com.xh.generate.bean;


import java.util.LinkedHashMap;
import java.util.List;


/**
* @Name TableInfo
* @Description 存储表结构的信息
* @Author wen
* @Date 2020/3/29
*/
public class TableInfo {
    /**
     * 表名
     */
    private String tname;
    /**
     * 所有字段的信息
     */
    private LinkedHashMap<String, ColumnInfo> columns;
    /**
     * 唯一主键(目前我们只能处理表中有且只有一个主键的情况)
     */
    private ColumnInfo onlyPriKey;
    /**
     * 如果联合主键，则在这里存储
     */
    private List<ColumnInfo> priKeys;
    /**
     * 主键字段java类型
     */
    private String priKeyFieldType;

    public TableInfo() {
    }

    public TableInfo(String tname, List<ColumnInfo> priKeys, LinkedHashMap<String, ColumnInfo> columns) {
        this.tname = tname;
        this.columns = columns;
        this.priKeys = priKeys;
    }

    public String getTname() {
        return tname;
    }

    public void setTname(String tname) {
        this.tname = tname;
    }

    public LinkedHashMap<String, ColumnInfo> getColumns() {
        return columns;
    }

    public void setColumns(LinkedHashMap<String, ColumnInfo> columns) {
        this.columns = columns;
    }

    public ColumnInfo getOnlyPriKey() {
        return onlyPriKey;
    }

    public void setOnlyPriKey(ColumnInfo onlyPriKey) {
        this.onlyPriKey = onlyPriKey;
    }

    public List<ColumnInfo> getPriKeys() {
        return priKeys;
    }

    public void setPriKeys(List<ColumnInfo> priKeys) {
        this.priKeys = priKeys;
    }

    public String getPriKeyFieldType() {
        return priKeyFieldType;
    }

    public void setPriKeyFieldType(String priKeyFieldType) {
        this.priKeyFieldType = priKeyFieldType;
    }
}
