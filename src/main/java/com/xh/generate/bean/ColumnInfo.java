package com.xh.generate.bean;



/**
* @Name
* @Description 封装表中一个字段的信息
* @Author wen
* @Date 2020/3/29
*/
public class ColumnInfo {
    /**
     * 字段名称
     */
    private Field field;
    /**
     * 字段的数据类型
     */
    private Type type;
    /**
     * 字段描述信息
     */
    private String remarks;
    /**
     * 字段的键类型(0：普通键，1：主键 2：外键)
     */
    private int keyType;
    /**
     * 字段递增(0：否，1：是)
     */
    private int autoincrement;


    public static class Field {
        /**
         * 数据库字段名称
         */
        private String columnName;
        /**
         * java po对象字段名称
         */
        private String fieldName;

        public Field(String columnName, String fieldName) {
            this.columnName = columnName;
            this.fieldName = fieldName;
        }

        public String getColumnName() {
            return columnName;
        }

        public String getFieldName() {
            return fieldName;
        }
    }

    public static class Type {
        /**
         * 数据库字段字段类型
         */
        private String columnType;
        /**
         * java po对象字段类型
         */
        private String fieldType;
        /**
         * java po对象字段类型
         */
        private String jdbcType;

        public Type(String columnType, String fieldType, String jdbcType) {
            this.columnType = columnType;
            this.fieldType = fieldType;
            this.jdbcType = jdbcType;
        }

        public String getColumnType() {
            return columnType;
        }

        public String getFieldType() {
            return fieldType;
        }

        public String getJdbcType() {
            return jdbcType;
        }
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public int getKeyType() {
        return keyType;
    }

    public void setKeyType(int keyType) {
        this.keyType = keyType;
    }

    public int getAutoincrement() {
        return autoincrement;
    }

    public void setAutoincrement(int autoincrement) {
        this.autoincrement = autoincrement;
    }
}





