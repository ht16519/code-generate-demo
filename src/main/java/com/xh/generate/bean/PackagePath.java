package com.xh.generate.bean;


/**
 * @Name packagePath
 * @Description 需要生成代码的所有类包路径
 * @Author wen
 * @Date 2020-03-30
 */
public class PackagePath {

    /**
     * 扫描生成java类的包 po(Persistence object持久化对象)
     */
    private String poPackage;
    /**
     * 设置姓名配置文件config的包路径
     * */
    private String configPackage;
    /**
     * 扫描生成java类的包 vo(View object前端视图对象)
     */
    private String voPackage;
    /**
     * 扫描生成java类的包 mapper
     */
    private String mapperPackage;
    /**
     * 扫描生成java类的包 service
     */
    private String servicePackage;
    /**
     * 扫描生成java类的包 controller
     */
    private String controllerPackage;
    /**
     * 扫描生成java类的包 commons
     */
    private String commonsPackage;
    /**
     * 扫描生成java类的包 message
     */
    private String messagePackage;

    public String getPoPackage() {
        return poPackage;
    }

    public void setPoPackage(String poPackage) {
        this.poPackage = poPackage;
    }

    public String getVoPackage() {
        return voPackage;
    }

    public void setVoPackage(String voPackage) {
        this.voPackage = voPackage;
    }

    public String getMapperPackage() {
        return mapperPackage;
    }

    public void setMapperPackage(String mapperPackage) {
        this.mapperPackage = mapperPackage;
    }

    public String getServicePackage() {
        return servicePackage;
    }

    public void setServicePackage(String servicePackage) {
        this.servicePackage = servicePackage;
    }

    public String getControllerPackage() {
        return controllerPackage;
    }

    public void setControllerPackage(String controllerPackage) {
        this.controllerPackage = controllerPackage;
    }

    public String getCommonsPackage() {
        return commonsPackage;
    }

    public void setCommonsPackage(String commonsPackage) {
        this.commonsPackage = commonsPackage;
    }

    public String getMessagePackage() {
        return messagePackage;
    }

    public void setMessagePackage(String messagePackage) {
        this.messagePackage = messagePackage;
    }

    public String getConfigPackage() {
        return configPackage;
    }

    public void setConfigPackage(String configPackage) {
        this.configPackage = configPackage;
    }
}
