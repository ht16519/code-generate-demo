package com.xh.generate.bean;

/**
 * @Name packagePath
 * @Description 需要生成代码的所有文件路径
 * @Author wen
 * @Date 2020-03-30
 */
public class CodeFilePath {

    /**
     * po(Persistence object持久化对象) 文件路径
     */
    private String poPath;

    /**
     * 配置文件路径
     * */
    private String configPath;
    /**
     * vo(View object前端视图对象) 文件路径
     */
    private String voPath;
    /**
     * mapper 文件路径
     */
    private String mapperPath;
    /**
     * mapper.xml 文件路径
     */
    private String mapperXmlPath;
    /**
     * service 文件路径
     */
    private String servicePath;
    /**
     * controller 文件路径
     */
    private String controllerPath;
    /**
     * 扫描生成java类的包 commons
     */
    private String commonsPath;
    /**
     * enums 文件路径
     */
    private String enumsPath;

    /**
     * 项目的resources 文件路径
     */
    private String resourcesPath;

    public String getPoPath() {
        return poPath;
    }

    public void setPoPath(String poPath) {
        this.poPath = poPath;
    }

    public String getVoPath() {
        return voPath;
    }

    public void setVoPath(String voPath) {
        this.voPath = voPath;
    }

    public String getMapperPath() {
        return mapperPath;
    }

    public void setMapperPath(String mapperPath) {
        this.mapperPath = mapperPath;
    }

    public String getMapperXmlPath() {
        return mapperXmlPath;
    }

    public void setMapperXmlPath(String mapperXmlPath) {
        this.mapperXmlPath = mapperXmlPath;
    }

    public String getServicePath() {
        return servicePath;
    }

    public void setServicePath(String servicePath) {
        this.servicePath = servicePath;
    }

    public String getControllerPath() {
        return controllerPath;
    }

    public void setControllerPath(String controllerPath) {
        this.controllerPath = controllerPath;
    }

    public String getCommonsPath() {
        return commonsPath;
    }

    public void setCommonsPath(String commonsPath) {
        this.commonsPath = commonsPath;
    }

    public String getEnumsPath() {
        return enumsPath;
    }

    public void setEnumsPath(String enumsPath) {
        this.enumsPath = enumsPath;
    }

    public String getResourcesPath() {
        return resourcesPath;
    }

    public void setResourcesPath(String resourcesPath) {
        this.resourcesPath = resourcesPath;
    }

    public String getConfigPath() {
        return configPath;
    }

    public void setConfigPath(String configPath) {
        this.configPath = configPath;
    }
}
