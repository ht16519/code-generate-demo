package com.xh.generate.core.convertor;

import java.util.Properties;

/**
 * @Name SimpleTypeConvertor
 * @Description 简单的类型转换
 * @Author wen
 * @Date 2020-03-30
 */
public class DatabaseType2JavaTypeConvertor extends AbstractTypeConvertor {

//    private final Properties pros = new Properties();

    public DatabaseType2JavaTypeConvertor() {
//        try {
//            pros.load(TypeConvertor.class.getClassLoader().getResourceAsStream("type_mapping.properties"));
//            if(pros.size() == 0){
//                init(pros);
//            }
//        } catch (Exception e) {
//            init(pros);
//        }
    }

    @Override
    public void initializationMapping(Properties pros) {
        pros.put("varchar", "String");
        pros.put("char", "String");
        pros.put("int", "Integer");
        pros.put("tinyint", "Integer");
        pros.put("smallint", "Integer");
        pros.put("integer", "Integer");
        pros.put("bigint", "Long");
        pros.put("double", "Double");
        pros.put("clob", "java.sql.CLob");
        pros.put("blob", "java.sql.BLob");
        pros.put("date", "java.sql.Date");
        pros.put("time", "java.sql.Time");
        pros.put("timestamp", "java.sql.Timestamp");
        pros.put("datetime", "Date");
        pros.put("text", "String");
    }

//    @Override
//    public String exec(String target) {
//        String property = pros.getProperty(target.toLowerCase());
//        if(StringUtil.isEmpty(property)){
//            throw new IllegalArgumentException("暂时不支持对数据库 " + target + " 进行类型转换");
//        }
//        return property;
//    }

    /**
    * @Name init
    * @Description 设置数据库和java po对象类型对应关系
    * @Author wen
    * @Date 2020/3/30
    * @return void
    */
//    public void init(Properties pros) {
//        pros.put("varchar", "String");
//        pros.put("char", "String");
//        pros.put("int", "Integer");
//        pros.put("tinyint", "Integer");
//        pros.put("smallint", "Integer");
//        pros.put("integer", "Integer");
//        pros.put("bigint", "Long");
//        pros.put("double", "Double");
//        pros.put("clob", "java.sql.CLob");
//        pros.put("blob", "java.sql.BLob");
//        pros.put("date", "java.sql.Date");
//        pros.put("time", "java.sql.Time");
//        pros.put("timestamp", "java.sql.Timestamp");
//        pros.put("datetime", "Date");
//    }

}
