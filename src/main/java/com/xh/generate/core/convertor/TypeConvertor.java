package com.xh.generate.core.convertor;

import java.util.Properties;

/**
* @Name TypeConvertor
* @Description 负责java数据类型和数据库数据类型的互相转换
* @Author wen
* @Date 2020/3/29
*/
public interface TypeConvertor {

	/**
	* @Name exec
	* @Description 类型转换执行方法
	* @Author wen
	* @Date 2020/3/31
//	* @param target
	* @return java.lang.String
	*/
//	default String exec(String target){
//		throw new RuntimeException("类型转换异常...");
//	}

	void initializationMapping(Properties pros);

	String exec(String target);
}
