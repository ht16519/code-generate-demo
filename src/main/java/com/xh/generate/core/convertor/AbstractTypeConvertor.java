package com.xh.generate.core.convertor;

import com.xh.generate.utils.StringUtil;
import java.util.Properties;

/**
 * @Name AbstractTypeConvertor
 * @Description
 * @Author wen
 * @Date 2020-03-31
 */
public abstract class AbstractTypeConvertor implements TypeConvertor {

    private final Properties pros = new Properties();

    public AbstractTypeConvertor() {
        try {
            pros.load(TypeConvertor.class.getClassLoader().getResourceAsStream("type_mapping.properties"));
            if(pros.size() == 0){
                initializationMapping(pros);
            }
        } catch (Exception e) {
            initializationMapping(pros);
        }
        initializationMapping(pros);
    }

    public abstract void initializationMapping(Properties pros);

    @Override
    public String exec(String target){
        String property = pros.getProperty(target.toLowerCase());
        if(StringUtil.isEmpty(property)){
            throw new IllegalArgumentException("暂时不支持对数据库 " + target + " 进行类型转换");
        }
        return property;
    }

}
