package com.xh.generate.core.convertor;

import java.util.Properties;

/**
 * @Name SimpleTypeConvertor
 * @Description 简单的类型转换
 * @Author wen
 * @Date 2020-03-30
 */
public class DatabaseType2JdbcTypeConvertor extends AbstractTypeConvertor {

    public DatabaseType2JdbcTypeConvertor() {
    }

    @Override
    public void initializationMapping(Properties pros) {
        pros.put("varchar", "VARCHAR");
        pros.put("char", "CHAR");
        pros.put("int", "INTEGER");
        pros.put("tinyint", "INTEGER");
        pros.put("smallint", "INTEGER");
        pros.put("integer", "INTEGER");
        pros.put("bigint", "BIGINT");
        pros.put("double", "DOUBLE");
        pros.put("clob", "CLOB");
        pros.put("blob", "BLOB");
        pros.put("date", "DATE");
        pros.put("time", "TIME");
        pros.put("timestamp", "TIMESTAMP");
        pros.put("datetime", "TIMESTAMP");
        pros.put("text", "LONGVARCHAR");
    }

}
