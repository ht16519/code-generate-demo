package com.xh.generate.core;

import com.xh.generate.bean.CodeFilePath;
import com.xh.generate.bean.Configuration;
import com.xh.generate.bean.PackagePath;
import com.xh.generate.utils.StringUtil;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

/**
 * @Name DBManager
 * @Description 根据配置信息，维持连接对象的管理
 * @Author wen
 * @Date 2020/3/29
 */
public class DBManager {
    /**
     * 配置信息
     */
    private static Configuration conf;

    static {
        Properties pros = new Properties();
        try {
            //加载配置
            pros.load(DBManager.class.getClassLoader().getResourceAsStream("db.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        //获取到的字节码根路径
        String classesPath = DBManager.class.getResource("/").getPath();
        String diskUrl = pros.getProperty("diskUrl");
        String rootPath;
        if(StringUtil.isEmpty(diskUrl)){
            //生成代码的项目根路径（src/java）
            rootPath = classesPath.replace("target/", "src/").replace("classes/", "main/java/");
        }else {
            rootPath = diskUrl;
        }

        //将配置信息读取到Configuration对象中
        conf = new Configuration();
        String basePackage = pros.getProperty("basePackage");
        conf.setBasePackage(StringUtil.isEmpty(basePackage) ? "com.xh.demo" : basePackage);
        conf.setDriver(pros.getProperty("driver"));
        conf.setUrl(pros.getProperty("url"));
        conf.setUsername(pros.getProperty("username"));
        conf.setPassword(pros.getProperty("password"));
        conf.setGenerateTables(pros.getProperty("generateTables"));
        conf.setIsGenerateCommonsCode(pros.getProperty("isGenerateCommonsCode"));
        conf.setIsGeneratePomDependenciesAndYml(pros.getProperty("isGeneratePomDependenciesAndYml"));
        conf.setTableNamePrefix(pros.getProperty("tableNamePrefix"));
        conf.setTypeConvertorClassPath(pros.getProperty("typeConvertorClassPath"));
        conf.setDiskUrl(diskUrl);
        conf.setClassesPath(classesPath);
        conf.setRootPath(rootPath);
        //保存需要生成代码的所有类包路径
        setPath(pros, basePackage, rootPath);
    }

    /**
    * @Name setPath
    * @Description 保存需要生成代码的所有类包路径
    * @Author wen
    * @Date 2020/3/31
     * @return void
    */
    private static void setPath(Properties pros, String basePackage, String rootPath) {
        //获取所有包路径
        String poPackage = StringUtil.compensatePackage(basePackage, pros.getProperty("poPackage"), ".domain.po");
        String voPackage = StringUtil.compensatePackage(basePackage, pros.getProperty("voPackage"), ".domain.vo");
        String mapperPackage = StringUtil.compensatePackage(basePackage, pros.getProperty("mapperPackage"), ".dao");
        String servicePackage = StringUtil.compensatePackage(basePackage, pros.getProperty("servicePackage"), ".service");
        String commonsPackage = StringUtil.compensatePackage(basePackage, pros.getProperty("commonsPackage"), ".commons");
        String controllerPackage = StringUtil.compensatePackage(basePackage, pros.getProperty("controllerPackage"), ".controller");
        String configPackage = StringUtil.compensatePackage(basePackage, pros.getProperty("configPackage"), ".config");
        //保存需要生成代码的所有包路径
        PackagePath packagePath = conf.getPackagePath();
        packagePath.setPoPackage(poPackage);
        packagePath.setVoPackage(voPackage);
        packagePath.setMapperPackage(mapperPackage);
        packagePath.setServicePackage(servicePackage);
        packagePath.setCommonsPackage(commonsPackage);
        packagePath.setControllerPackage(controllerPackage);
        packagePath.setConfigPackage(configPackage);
        //保存需要生成代码的所有文件路径
        CodeFilePath codeFilePath = conf.getCodeFilePath();
        codeFilePath.setPoPath(StringUtil.packagePath2FilePath(rootPath, poPackage));
        codeFilePath.setConfigPath(StringUtil.packagePath2FilePath(rootPath, configPackage));
        codeFilePath.setVoPath(StringUtil.packagePath2FilePath(rootPath, voPackage));
        codeFilePath.setMapperPath(StringUtil.packagePath2FilePath(rootPath, mapperPackage));
        codeFilePath.setServicePath(StringUtil.packagePath2FilePath(rootPath, servicePackage));
        codeFilePath.setCommonsPath(StringUtil.packagePath2FilePath(rootPath, commonsPackage));
        codeFilePath.setControllerPath(StringUtil.packagePath2FilePath(rootPath, controllerPackage));
        //项目的resources 文件路径
        String resourcesPath;
        String diskUrl = conf.getDiskUrl();
        if(StringUtil.isEmpty(diskUrl)){
            resourcesPath = conf.getClassesPath().replace("target/", "src/").replace("classes/", "main/resources/");
        }else {
            resourcesPath = diskUrl + "resources/";
        }
        codeFilePath.setResourcesPath(resourcesPath);
        //生成mapper.xml代码的项目根路径（src/resources/mappers）
        codeFilePath.setMapperXmlPath(resourcesPath + "mappers/");
    }

    /**
     * 获取Connection对象
     *
     * @return
     */
    public static Connection getConnection() {
        try {
            Class.forName(conf.getDriver());
            return DriverManager.getConnection(conf.getUrl(),
                    conf.getUsername(), conf.getPassword());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 返回Configuration对象
     *
     * @return
     */
    public static Configuration getConf() {
        return conf;
    }


}
