package com.xh.generate.core;

import com.xh.generate.bean.CodeFilePath;
import com.xh.generate.bean.Configuration;
import com.xh.generate.bean.PackagePath;
import com.xh.generate.utils.DataMapUtil;
import com.xh.generate.utils.FileUtil;
import com.xh.generate.utils.GenerateUtil;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @Name PlugsGenerateService
 * @Description
 * @Author wen
 * @Date 2020-03-29
 */
public class PlugsGenerater {

    private static final String FLAG_FALSE = "false";

    private static final Configuration conf = DBManager.getConf();

    private static final PackagePath packagePath = conf.getPackagePath();

    private static final CodeFilePath codeFilePath = conf.getCodeFilePath();

    private static final Map<String, Object> data;

    static {
        data = DataMapUtil.DATA;
        data.put("commonPackage", packagePath.getCommonsPackage());
    }

    public static void generateApiResult() throws IOException {
        String voPath = codeFilePath.getVoPath();
        String apiResultFile = voPath + "ApiResult.java";
        if (!new File(apiResultFile).exists()) {
            String enumsPath = codeFilePath.getCommonsPath() + "enums/";
            FileUtil.createDirectory(voPath);
            FileUtil.createDirectory(enumsPath);
            //生成文件全路径
            String commomMessagePathFile = voPath + "CommomMessage.java";
            String messageEnumPathFile = enumsPath + "MessageEnum.java";
            //生成ApiResult.java文件
            GenerateUtil.export(apiResultFile, "plugs/ApiResult", data);
            GenerateUtil.export(messageEnumPathFile, "plugs/MessageEnum", data);
            GenerateUtil.export(commomMessagePathFile, "plugs/CommonMessage", data);

            System.out.println(" ApiResult CODE BUILD SUCCESS!!! ");
        }
    }

    public static void generateCommons() throws IOException {
        if (!FLAG_FALSE.equalsIgnoreCase(conf.getIsGenerateCommonsCode())) {
            String exceptionPath = codeFilePath.getCommonsPath() + "exception/";
            String handlePath = codeFilePath.getCommonsPath() + "handle/";
            String utilsPath = codeFilePath.getCommonsPath() + "utils/";

            //生成文件全路径
            String voPath = codeFilePath.getVoPath();
            String exResultPathFile = voPath + "ExResult.java";
            String jsonUtilPathFile = utilsPath + "JsonUtil.java";
            String ResponseUtilFile = utilsPath + "ResponseUtil.java";
            String businessExceptionPathFile = exceptionPath + "BusinessException.java";
            String globalExceptionHandlerPathFile = handlePath + "GlobalExceptionHandler.java";
            //生成commons的文件
            String commonsPackage = packagePath.getCommonsPackage();

            if (!new File(jsonUtilPathFile).exists()) {
                FileUtil.createDirectory(utilsPath);
                GenerateUtil.export(jsonUtilPathFile, "plugs/JsonUtil", data);
            }

            if (!new File(ResponseUtilFile).exists()) {
                GenerateUtil.export(ResponseUtilFile, "plugs/ResponseUtil", data);
            }

            if (!new File(exResultPathFile).exists()) {
                GenerateUtil.export(exResultPathFile, "plugs/ExResult", data);
            }

            if (!new File(businessExceptionPathFile).exists()) {
                FileUtil.createDirectory(exceptionPath);
                GenerateUtil.export(businessExceptionPathFile, "plugs/BusinessException", data);
            }

            if (!new File(globalExceptionHandlerPathFile).exists()) {
                data.put("handlePackage", commonsPackage + ".handle");
                FileUtil.createDirectory(handlePath);
                GenerateUtil.export(globalExceptionHandlerPathFile, "plugs/GlobalExceptionHandler", data);
                System.out.println(" Commons CODE BUILD SUCCESS!!! ");
            }
        }

    }

    public static void generatePomAndYml() throws IOException {
        if (!FLAG_FALSE.equalsIgnoreCase(conf.getIsGeneratePomDependenciesAndYml())) {
            String resourcesPath = codeFilePath.getResourcesPath();
            if (!new File(resourcesPath).exists()) {
                FileUtil.createDirectory(resourcesPath);
            }

            String pomFile = resourcesPath + "the_test_pom.xml";
            if (!new File(pomFile).exists()) {
                GenerateUtil.export(pomFile, "properties/pom", data);
                System.out.println(" Commons 【the_test_pom.xml】 BUILD SUCCESS!!! ");
            }

            String ymlFile = resourcesPath + "application.yml";
            if (!new File(ymlFile).exists()) {
                GenerateUtil.export(ymlFile, "properties/yml", data);
                System.out.println(" Commons 【application.yml】 BUILD SUCCESS!!! ");
            }
        }
    }

}
