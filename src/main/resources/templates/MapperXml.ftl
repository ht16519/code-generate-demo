<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="${mapperPackage}.${poClassName}Mapper" >

    <resultMap id="BaseResultMap" type="${poPackage}.${poClassName}" >
    <#list columns as item>
        <#assign idField = item.field.columnName/>
        <#if item.keyType == 1>
        <id column="${item.field.columnName!""}" property="${item.field.fieldName}" jdbcType="${item.type.jdbcType?upper_case}" />
        <#else>
        <result column="${item.field.columnName!""}" property="${item.field.fieldName}" jdbcType="${item.type.jdbcType?upper_case}" />
        </#if>
    </#list>
    </resultMap>

    <#if priKeyJavaType??>
    <delete id="batchDelete">
        DELETE FROM
            ${tableName}
        WHERE
            ${idField} IN
        <foreach collection="list" item="id" open="(" separator="," close=")">
            <#noparse>#{id}</#noparse>
        </foreach>
    </delete>
    </#if>

</mapper>