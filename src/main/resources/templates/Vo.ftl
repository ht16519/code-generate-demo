package ${poPackage};

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ${voClassName} implements Serializable{

    private static final long serialVersionUID = 1L;

    <#list columns as item>
    private ${item.type.fieldType!""} ${item.field.fieldName};

    </#list>

}