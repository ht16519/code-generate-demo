package ${poPackage};

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "${tableName}")
public class ${poClassName} implements Serializable{

    private static final long serialVersionUID = 1L;

    <#list columns as item>
    /**
     * ${item.remarks!""}
     */
    <#if item.keyType == 1>
    @Id
    </#if>
    <#if item.autoincrement == 1>
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    </#if>
    <#if (item.field.columnName?length != item.field.fieldName?length)>
    @Column(name = "${item.field.columnName}")
    </#if>
    private ${item.type.fieldType!""} ${item.field.fieldName};

    </#list>

}