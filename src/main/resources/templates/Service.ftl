package ${servicePackage};

import ${poPackage}.${poClassName};
import java.lang.*;

public interface ${poClassName}Service {

    /**
    * @name add
    * @description 新增操作
    * @return int
    */
    int add(${poClassName} ${poInstance});

    <#if priKeyJavaType??>
    /**
    * @name getById
    * @description 通过id获取单条数据
    * @param id
    */
    ${poClassName} getById(${priKeyJavaType} id);
    </#if>

    /**
    * @name findOne
    * @description 条件查询一条数据
    * @param ${poInstance}
    */
    ${poClassName} findOne(${poClassName} ${poInstance});

    /**
    * @name update
    * @description 修改数据
    * @return int
    */
    int update(${poClassName} ${poInstance});

    <#if priKeyJavaType??>
    /**
    * @name removeById
    * @description 删除（物理删除）
    * @param id
    * @return int
    */
    int deleteById(${priKeyJavaType} id);
    </#if>

    <#--/**-->
    <#--* @name removeById-->
    <#--* @description 移除（逻辑删除）-->
    <#--* @param id-->
    <#--* @return int-->
    <#--*/-->
    <#--int removeById(Serializable id);-->
}
