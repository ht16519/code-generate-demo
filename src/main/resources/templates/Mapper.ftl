package ${mapperPackage};

import ${baseMapperPackage}.IBaseMapper;
import ${poPackage}.${poClassName};
import java.util.List;

public interface ${poClassName}Mapper extends IBaseMapper<${poClassName}> {
    <#if priKeyJavaType??>
    /**
     * @Name batchDelete
     * @Description 批量删除
     */
    int batchDelete(List<${priKeyJavaType}> ids);
    </#if>
}