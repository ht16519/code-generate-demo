package ${configPackage};

import org.springframework.context.annotation.Configuration;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @Name MybatisConfig
 * @Description MybatisConfig配置文件
 */
@Configuration
@MapperScan(basePackages = {"${mapperPackage}"})
public class MybatisConfig {
}
