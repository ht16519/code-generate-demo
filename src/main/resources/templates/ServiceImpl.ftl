package ${servicePackage}.impl;

<#--import java.io.Serializable;-->
import lombok.extern.slf4j.Slf4j;
import ${poPackage}.${poClassName};
import ${exceptionPackage}.BusinessException;
import ${enumsPackage}.MessageEnum;
<#--import ${voPackage}.${voClassName};-->
import ${mapperPackage}.${poClassName}Mapper;
import ${servicePackage}.${poClassName}Service;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
@Service
public class ${poClassName}ServiceImpl implements ${poClassName}Service {

    @Autowired
    private ${poClassName}Mapper ${poInstance}Mapper;

    <#if priKeyJavaType??>
    @Override
    public ${poClassName} getById(${priKeyJavaType} id){
        return ${poInstance}Mapper.selectByPrimaryKey(id);
    }
    </#if>

    @Override
    public ${poClassName} findOne(${poClassName} ${poInstance}){
        return ${poInstance}Mapper.selectOne(${poInstance});
    }

    @Override
    public int add(${poClassName} ${poInstance}){
        return ${poInstance}Mapper.insertSelective(${poInstance});
    }

    @Override
    public int update(${poClassName} ${poInstance}){
        if(${poInstance}.getId() == null){
            throw BusinessException.build(MessageEnum.PARAMETER_VERIFICATION_ERROR_ID);
        }
        return ${poInstance}Mapper.updateByPrimaryKeySelective(${poInstance});
    }

    <#if priKeyJavaType??>
    @Override
    public int deleteById(${priKeyJavaType} id){
        return ${poInstance}Mapper.deleteByPrimaryKey(id);
    }
    </#if>

    <#--@Override-->
    <#--int removeById(Serializable id){-->
        <#--return ${poInstance}Mapper.updateByPrimaryKeySelective(${poClassName}.builder().status(-1).build());-->
    <#--}-->

}
