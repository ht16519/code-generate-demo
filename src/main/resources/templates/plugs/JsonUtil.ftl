package ${commonPackage}.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.extern.slf4j.Slf4j;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @Name JsonUtil
 * @Description JsonUtil工具
 */
@Slf4j
public class JsonUtil {

    public static final ObjectMapper MAPPER = new ObjectMapper();

    static {
////         是否缩放排列输出，默认false，有些场合为了便于排版阅读则需要对输出做缩放排列
//        MAPPER.enable(SerializationFeature.INDENT_OUTPUT);
////         序列化日期时以timestamps输出，默认true
//        MAPPER.enable(SerializationFeature.INDENT_OUTPUT);
////         序列化枚举是以toString()来输出，默认false，即默认以name()来输出     如：ENUM01("enum_01") 默认为 ENUM01，true则为enum_01
//        MAPPER.enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING);
////         序列化枚举是以ordinal()来输出，默认false   如：ENUM01("enum_01") 默认为 ENUM01，true则为 0 非常危险
//        MAPPER.enable(SerializationFeature.WRITE_ENUMS_USING_INDEX);
////         序列化Map时对key进行排序操作，默认false
//        MAPPER.enable(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS);
////         序列化char[]时以json数组输出，默认false
//        MAPPER.enable(SerializationFeature.WRITE_CHAR_ARRAYS_AS_JSON_ARRAYS);
////        序列化BigDecimal时之间输出原始数字还是科学计数，默认false，即是否以toPlainString()科学计数方式来输出
////         1E+20 -> 100000000000000000000
//        MAPPER.enable(JsonGenerator.Feature.WRITE_BIGDECIMAL_AS_PLAIN);
////        禁用序列化日期为timestamps
//        MAPPER.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
////         视空字符传为null
//        MAPPER.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
////         低层级配置
//        MAPPER.configure(JsonParser.Feature.ALLOW_COMMENTS, true);
////         允许属性名称没有引号
//        MAPPER.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
////         允许单引号
//        MAPPER.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);

        // 如果为空则不输出
        MAPPER.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        // 对于空的对象转json的时候不抛出错误
        MAPPER.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        // 禁用遇到未知属性抛出异常
        MAPPER.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        // 取消对非ASCII字符的转码
        MAPPER.configure(JsonGenerator.Feature.ESCAPE_NON_ASCII, false);
    }

    /**
     * @param obj
     * @return java.lang.String
     * @Name serialize
     * @Description 序列化对象（转json）
     */
    public static String serialize(Object obj) {
        if (obj == null) {
            return null;
        }
        if (obj.getClass() == String.class) {
            return (String) obj;
        }
        try {
            return MAPPER.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            log.error("json序列化出错：" + obj, e);
            return null;
        }
    }

    /**
     * @param json
     * @param tClass
     * @return T
     * @Name parse
     * @Description 反序列化（json转为Bean）
     */
    public static <T> T parse(String json, Class<T> tClass) {
        try {
            return MAPPER.readValue(json, tClass);
        } catch (IOException e) {
            log.error("json解析出错：" + json, e);
            return null;
        }
    }

    /**
    * @param json
    * @param eClass
    * @return java.util.List<E>
    * @Name parseList
    * @Description 反序列化（json转List）
    */
    public static <E> List<E> parseList(String json, Class<E> eClass) {
        try {
            return MAPPER.readValue(json, MAPPER.getTypeFactory().constructCollectionType(List.class, eClass));
        } catch (IOException e) {
            log.error("json解析出错：" + json, e);
            return null;
        }
    }

    /**
    * @param json
    * @param kClass
    * @param vClass
    * @return java.util.Map<K, V>
    * @Name parseMap
    * @Description 反序列化（json转Map）
    */
    public static <K, V> Map<K, V> parseMap(String json, Class<K> kClass, Class<V> vClass) {
        try {
            return MAPPER.readValue(json, MAPPER.getTypeFactory().constructMapType(Map.class, kClass, vClass));
        } catch (IOException e) {
            log.error("json解析出错：" + json, e);
            return null;
        }
    }

    /**
    * @param json
    * @param type
    * @return T
    * @Name nativeRead
    * @Description json转复杂对象
    */
    public static <T> T nativeRead(String json, TypeReference<T> type) {
        try {
            return MAPPER.readValue(json, type);
        } catch (IOException e) {
            log.error("json解析出错：" + json, e);
            return null;
        }
    }

//============================测试使用========

//    @Data
//    @AllArgsConstructor
//    @NoArgsConstructor
//    static class User{
//        private Integer id;
//        private String name;
//        private Double age;
//    }
//
//    @Data
//    @AllArgsConstructor
//    @NoArgsConstructor
//    @JsonIgnoreProperties(ignoreUnknown = true)
//    static class W{
//        private List<Object> lives;
//    }
//
//    public static void main(String[] args) {
//        User user = new User(1, "jack", 16.5);
//        //序列化对象（转json）
//        String json = serialize(user);
//        System.out.println(json);
//
//        //反序列化（json转为Bean）
//        User u = parse(json, User.class);
//        System.out.println(u);
//
//        //反序列化（json转List）
//        json = "[20, 15, -1, 22]";
//        System.out.println(parseList(json, String.class));
//        System.out.println(parseList(json, Integer.class));
//
//        //反序列化（json转Map）
//        json = "{\"name\" : \"历史\", \"id\":123, \"age\":12.2}";
//        Map<Object, Object> map = parseMap(json, Object.class, Object.class);
//        System.out.println(map);
//
//
//
//        String j = "{\"status\": \"1\",\"count\": \"1\",\"info\": \"OK\",\"infocode\": \"10000\",\"lives\": [{\"province\": \"河北\",\"city\": \"张家口市\",\"adcode\": \"130700\",\"weather\": \"晴\",\"temperature\": \"15\",\"winddirection\": \"西南\",\"windpower\": \"≤3\",\"humidity\": \"11\",\"reporttime\": \"2019-04-14 11:20:08\"}]}";
//        Map<String, Object> stringObjectMap = parseMap(j, String.class, Object.class);
//        System.out.println(stringObjectMap);
//
//        System.out.println(parse(j, W.class));
//
//
//        //json转复杂对象
//        json = "[{\"name\":\"李四\", \"age\":12.3}, {\"name\":\"王文\", \"age\":16.3}]";
//        List<Map<String, String>> maps = nativeRead(json, new TypeReference<List<Map<String, String>>>() {
//        });
//        System.out.println(maps);
//
//        System.err.println(serialize(null));
//    }
}
