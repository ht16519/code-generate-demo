package ${commonPackage}.utils;

import org.springframework.beans.BeanUtils;

/**
 * @Name BeanUtil
 * @Description bena工具类
 */
public class BeanUtil {

    private static <T> T convert(Object source, T target){
        BeanUtils.copyProperties(source, target);
        return target;
    }

}
