package ${voPackage};

/**
 * @Name CommomMessage
 * @Description 公共信息接口
 */
public interface CommomMessage {

	int getCode();

	String getMsg();

}
