package ${controllerPackage};

import java.util.List;
import ${voPackage}.ApiResult;
import lombok.extern.slf4j.Slf4j;
import ${poPackage}.${poClassName};
import ${servicePackage}.${poClassName}Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/${poInstance}")
public class ${poClassName}Controller {

    @Autowired
    private ${poClassName}Service ${poInstance}Service;
    <#if priKeyJavaType??>

    /**
    * @Name get
    * @Description 通过id获取数据
    * @param id
    */
    @GetMapping("/{id}")
    public ApiResult<${poClassName}> get(@PathVariable("id") ${priKeyJavaType} id) {
        return ApiResult.success(${poInstance}Service.getById(id));
    }
    </#if>

    /**
     * @Name add
     * @Description 保存操作
     * @param ${poInstance}
     */
    @PostMapping("/save")
    public ApiResult save(@RequestBody ${poClassName} ${poInstance}) {
        //添加操作
        return ApiResult.res(${poInstance}Service.add(${poInstance}));
    }

    /**
    * @Name update
    * @Description 修改操作
    * @param ${poInstance}
    */
    @PostMapping("/update")
    public ApiResult update(@RequestBody ${poClassName} ${poInstance}) {
        //修改操作
        return ApiResult.res(${poInstance}Service.update(${poInstance}));
    }

    <#if priKeyJavaType??>
    /**
    * @Name delete
    * @Description 删除操作
    * @param id
    */
    @GetMapping("/delete/{id}")
    public ApiResult<${poClassName}> delete(@PathVariable("id") ${priKeyJavaType} id) {
        ${poInstance}Service.deleteById(id);
        return ApiResult.success();
    }
    </#if>
    <#--<#if priKeyJavaType??>-->
    <#--/**-->
    <#--* @Name batchDelete-->
    <#--* @Description 批量删除操作-->
    <#--* @param id-->
    <#--*/-->
    <#--@GetMapping("delete/{id}")-->
    <#--public ApiResult<${poClassName}> batchDelete(@PathVariable("id") ${priKeyJavaType} id) {-->
        <#--${poInstance}Service.batchDeleteByIds(id);-->
        <#--return ApiResult.success();-->
    <#--}-->
    <#--</#if>-->

    <#--@PostMapping("save")-->
    <#--public ApiResult<${poClassName}> save(@RequestBody ${poClassName} ${poInstance}) {-->
    <#--if(${poInstance}.getId() == null){-->
    <#--${poInstance}Service.add(${poInstance});          //添加操作-->
    <#--} else {-->
    <#--${poInstance}Service.update(${poInstance});       //修改操作-->
    <#--}-->
    <#--return ApiResult.success();-->
    <#--}-->
}
